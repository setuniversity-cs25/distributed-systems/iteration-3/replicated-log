This is master server of "replicated log" system.
It runs on :8080 port

To store message, make POST request on localhost:8080/api/replicated-log/v1/message-memory
with JSON body in such format:

{
    "message": "test message 1"
}

To receive all stored messages in master server, make GET request on localhost:8080/api/replicated-log/v1/message-memory

ITERATION 3
To send write concern parameter, add ?w={x} to the URL, where {x} place values 1 - 3.
Ex.: localhost:8080/api/replicated-log/v1/message-memory?w=1

Added config file, where nodes can be specified.
Additional features implemented - heartbeats and quorum append.
If some secondary node registered as not healthy - no requests will be sent to it until its status returns to healthy.
Quorum is calculated as "not less than half of registered secondary nodes are available (healthy or suspected)".
E.g. if 2 secondary nodes are registered - at least 1 must be available. If 3 are registered - at least 2 must be available.
