package configs

type Config struct {
	ReplicaDomains       []string
	StoreMessagePathname string
	HealthInfoPathname   string
}

// Assign here proper configs. If running locally (not in Docker container),
// replace replicas domains with proper values (e.g. localhost:8081)
func GetDefaultConfig() Config {
	return Config{
		ReplicaDomains:       []string{"replicated-log-secondary1:8081", "replicated-log-secondary2:8082"},
		StoreMessagePathname: "/api/replicated-log/v1/message-memory",
		HealthInfoPathname:   "/api/replicated-log/v1/health",
	}
}
