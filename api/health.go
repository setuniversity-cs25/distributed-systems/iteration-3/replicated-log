package api

import (
	"log"
	"net/http"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
)

type healthOption string

type HealthStatus struct {
	Info      string       `json:"info"`
	Status    healthOption `json:"status"`
	Timestamp time.Time    `json:"timestamp"`
}

const (
	healthy   healthOption = "Healthy"
	suspected healthOption = "Suspected"
	unhealthy healthOption = "Unhealthy"
)

var (
	nodeStatusMap = map[string]healthOption{}
	mu            sync.RWMutex
)

func getHealthStatus(c *gin.Context) {
	healthStatuses, err := CheckHealthStatus()
	if err != nil {
		c.JSON(http.StatusInternalServerError, nil)
		return
	}

	c.JSON(http.StatusOK, healthStatuses)
}

func CheckHealthStatus() ([]HealthStatus, error) {
	healthStatuses := make([]HealthStatus, 0)

	healthStatus := HealthStatus{
		Info:      "Replicated Log Master Node",
		Status:    healthy,
		Timestamp: time.Now(),
	}

	healthStatuses = append(healthStatuses, healthStatus)

	healthStatuses, err := getReplicasStatus(healthStatuses)
	if err != nil {
		return []HealthStatus{}, err
	}

	for _, healthStatus := range healthStatuses {
		if nodeStatusMap[healthStatus.Info] != healthStatus.Status {
			log.Printf("Node: %s\nStatus: %s\nTime: %s\n\n", healthStatus.Info, healthStatus.Status, healthStatus.Timestamp)
			mu.Lock()
			nodeStatusMap[healthStatus.Info] = healthStatus.Status
			mu.Unlock()
		}
	}

	return healthStatuses, nil
}
