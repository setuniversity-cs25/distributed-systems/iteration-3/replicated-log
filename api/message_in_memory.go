package api

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"

	"gitlab.com/setuniversity-cs25/distributed-systems/replicated-log/configs"
	"gitlab.com/setuniversity-cs25/distributed-systems/replicated-log/models"
)

func messageInMemoryGetAll(c *gin.Context) {
	storedMessages := models.GetAllMessages()

	c.JSON(http.StatusOK, storedMessages)
}

func messageInMemoryCreate(c *gin.Context) {
	log.Println("Master storing message started")

	writeParam, err := strconv.ParseInt(c.Request.URL.Query().Get("w"), 10, 32)
	if err != nil {
		log.Println(err)
		c.JSON(http.StatusBadRequest, apiBadRequest(err.Error()))
		return
	}

	numberOfReplicas := len(configs.GetDefaultConfig().ReplicaDomains)

	// to avoid infinite loop
	if writeParam > int64(numberOfReplicas)+1 || writeParam < 1 {
		log.Println("too big parameter")
		c.JSON(http.StatusBadRequest, apiBadRequest(fmt.Sprintf("there are only %d nodes exist, write parameter should be not greater than %d and not less than 1", numberOfReplicas+1, numberOfReplicas+1)))
		return
	}

	var message models.Message
	if err := c.ShouldBindJSON(&message); err != nil {
		log.Println("error occured on parsing in messageInMemoryCreate")
		c.Error(err)
		return
	}

	message.CreatedAt = time.Now()

	createdMessage := models.CreateMessage(message)
	log.Println("Master message stored")

	err = makeReplicas(c, createdMessage, int(writeParam))
	if err != nil {
		c.Error(err)
		return
	}

	log.Println("Master storing message finished")
	c.JSON(http.StatusCreated, createdMessage)
}
