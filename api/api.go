package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type ErrorResponse struct {
	Type    string `json:"type"`
	Message string `json:"message"`
}

func apiBadRequest(message string) ErrorResponse {
	apiBadRequest := ErrorResponse{
		Type:    http.StatusText(http.StatusBadRequest),
		Message: message,
	}

	return apiBadRequest
}

func apiForbidden(message string) ErrorResponse {
	apiForbidden := ErrorResponse{
		Type:    http.StatusText(http.StatusForbidden),
		Message: message,
	}

	return apiForbidden
}

func Init(r *gin.Engine) {
	apiRouter := r.Group("/api/replicated-log/v1")
	apiRouter.Use(quorumMiddleware())

	health := apiRouter.Group("health")
	health.GET("", getHealthStatus)

	messageInMemory := apiRouter.Group("message-memory")
	messageInMemory.GET("", messageInMemoryGetAll)
	messageInMemory.POST("", messageInMemoryCreate)

}
