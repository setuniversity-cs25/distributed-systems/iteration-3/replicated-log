package api

import (
	"math"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/setuniversity-cs25/distributed-systems/replicated-log/configs"
)

// Checks if quorum exists. For quorum necessary that not less than half of secondary nodes had status healthy
func quorumMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		if c.Request.Method == http.MethodPost {
			mu.RLock()
			defer mu.RUnlock()

			numOfNodes := len(configs.GetDefaultConfig().ReplicaDomains)

			// Round to bigger value. If 2 nodes, quorum is 1. If 3 nodes, quorum is 2
			quorum := int(math.Round(float64(numOfNodes) / 2))
			counterUnavailable := 0

			for _, replicaDomain := range configs.GetDefaultConfig().ReplicaDomains {
				if nodeStatusMap[replicaDomain] == unhealthy {
					counterUnavailable++
					if counterUnavailable > numOfNodes-quorum {
						c.AbortWithStatusJSON(http.StatusForbidden, apiForbidden("write is not allowed: some nodes are unavailable: no quorum"))
						return
					}
				}
			}
		}
	}
}
