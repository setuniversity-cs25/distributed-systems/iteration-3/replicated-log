package api

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"

	"gitlab.com/setuniversity-cs25/distributed-systems/replicated-log/configs"
	"gitlab.com/setuniversity-cs25/distributed-systems/replicated-log/models"
)

func makeReplicas(c *gin.Context, message models.Message, writeParam int) error {

	defaultConfig := configs.GetDefaultConfig()

	jsonData, err := json.Marshal(message)
	if err != nil {
		return err
	}

	ackChan := make(chan bool)

	for index, replicaDomain := range defaultConfig.ReplicaDomains {
		go func(index int, replicaDomain string) {
			url := "http://" + replicaDomain + defaultConfig.StoreMessagePathname

			client := &http.Client{}
			sendAttempt := 0
			for {
				// Do not send request on not healthy node
				mu.RLock()
				if nodeStatusMap[replicaDomain] != healthy {
					mu.RUnlock()
					continue
				}
				mu.RUnlock()
				sendAttempt++

				req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(jsonData))
				if err != nil {
					log.Panicf(err.Error())
				}
				req.Header.Set("Content-Type", "application/json")

				log.Printf("Master is sending request to Secondary %d, domain: %s, attempt: %d\n", index+1, replicaDomain, sendAttempt)
				resp, err := client.Do(req)

				log.Printf("Master received response from Secondary %d, domain: %s, attempt: %d\n", index+1, replicaDomain, sendAttempt)
				if err != nil {
					log.Printf("Request to Secondary %d, domain: %s, attempt: %d, failed with error: %s\nNext attempt in %d seconds", index+1, replicaDomain, sendAttempt, err.Error(), sendAttempt)

					time.Sleep(time.Duration(sendAttempt) * time.Second)

					continue
				}

				if resp.StatusCode != http.StatusCreated {
					log.Printf("Request to Secondary %d, domain: %s, attempt: %d, failed with status: %s\nNext attempt in %d seconds", index+1, replicaDomain, sendAttempt, resp.Status, sendAttempt)
					resp.Body.Close()

					time.Sleep(time.Duration(sendAttempt) * time.Second)
					continue
				}

				resp.Body.Close()

				break
			}

			ackChan <- true

		}(index, replicaDomain)
	}

	// logic for waiting response from necessary amount of replicas
	for i := 1; i < writeParam; i++ {
		<-ackChan
	}

	return nil
}

func getReplicasStatus(healthStatuses []HealthStatus) ([]HealthStatus, error) {

	defaultConfig := configs.GetDefaultConfig()

	for _, replicaDomain := range defaultConfig.ReplicaDomains {
		url := "http://" + replicaDomain + defaultConfig.HealthInfoPathname

		client := &http.Client{}
		healthStatus := HealthStatus{}

		reqStartTime := time.Now()
		resp, err := client.Get(url)
		reqExecDuration := time.Since(reqStartTime)
		if err != nil || resp.StatusCode != http.StatusOK {
			healthStatus = HealthStatus{
				Info:      replicaDomain,
				Status:    unhealthy,
				Timestamp: time.Now(),
			}
		} else {
			jsonDecoder := json.NewDecoder(resp.Body)

			err = jsonDecoder.Decode(&healthStatus)
			if err != nil {
				return nil, err
			}
			resp.Body.Close()

			healthStatus.Info = replicaDomain

			if reqExecDuration > 5*time.Second {
				healthStatus.Status = suspected
			}
		}

		healthStatuses = append(healthStatuses, healthStatus)
	}

	return healthStatuses, nil
}
